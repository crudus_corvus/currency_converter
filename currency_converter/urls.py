from django.conf.urls import include, url
from django.contrib import admin
from core import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.MainView.as_view(), name='welcome'),
    url(r'^(?P<val>.+)\/(?P<code1>.+)\/to\/(?P<code2>.+)\/in\/html\/$', views.InHTMLView.as_view(), name='in_html'),
    url(r'^(?P<val>.+)\/(?P<code1>.+)\/to\/(?P<code2>.+)\/in\/json\/$', views.InJsonView.as_view(), name='in_json'),
    url(r'^(?P<val>.+)\/(?P<code1>.+)\/to\/(?P<code2>.+)\/in\/txt\/$', views.InTxtView.as_view(), name='in_txt'),
]
