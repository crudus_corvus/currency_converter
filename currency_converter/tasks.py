import logging
from celery.task import periodic_task
from core.utils import update

logger = logging.getLogger('converter.update')


@periodic_task(ignore_result=True, run_every=3600)
def update_task():
    """Celery task for updating db every hour with logger.
	"""

    logger.info("Starting update...")
    try:
        update()
    except Exception as e:
        logger.error("Error!! {0}: {1}".format(type(e), e))
    else:
        logger.info("Updated.")
