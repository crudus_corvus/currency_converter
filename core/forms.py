from django import forms
from .models import Currency


class SubmitForm(forms.Form):
    CHOICES = Currency.objects.values_list('code', 'code')
    val = forms.FloatField(required=True)
    val2 = forms.FloatField(required=False)
    curr1 = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=CHOICES)
    curr2 = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=CHOICES)

    def clean(self):
        cleaned_data = super(SubmitForm, self).clean()
        return cleaned_data
