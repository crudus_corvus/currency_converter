# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('code', models.CharField(max_length=3, db_index=True, verbose_name='currency code', unique=True)),
                ('value', models.FloatField(verbose_name='currency ratio')),
                ('name', models.CharField(max_length=170, verbose_name='currency name', unique=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'currencies',
                'ordering': ['code'],
                'verbose_name': 'currency',
            },
        ),
    ]
