from django.db import models


class Currency(models.Model):
    code = models.CharField('currency code', max_length=3,
                            unique=True, db_index=True)
    value = models.FloatField('currency ratio')
    name = models.CharField('currency name', max_length=170, unique=True)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["code"]
        verbose_name = 'currency'
        verbose_name_plural = 'currencies'

    def __str__(self):
        return self.code
