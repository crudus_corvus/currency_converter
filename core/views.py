from django.views import generic
from django.http import HttpResponse, JsonResponse, Http404, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.core.exceptions import SuspiciousOperation
from django.shortcuts import get_object_or_404
from .models import Currency
from .forms import SubmitForm


class MainView(generic.FormView):
	template_name = "core/base.html"
	form_class = SubmitForm

	def form_valid(self, form):
		return HttpResponseRedirect(reverse('in_html', kwargs={'val': form.cleaned_data['val'],
		                                                       'code1': form.cleaned_data['curr1'],
		                                                       'code2': form.cleaned_data['curr2']}))


class LogicMixin(object):
	"""Generates result and stores ratios in cache.

	Mixin for InTxt, InHTML and InJson views
	to comply DRY.

	Keyword arguments:
	val -- convertible value
	curr1, curr2 -- currencies objects
	val1, val2 -- currencies ratios
	result -- result of conversion
	"""

	def get_result(self):
		cache_key = (self.kwargs['code1'] + self.kwargs['code2']).upper()
		try:
			val = float(self.kwargs['val'])
		except ValueError:
			raise SuspiciousOperation('Invalid request: wrong value format')
		self.cache_dict = cache.get(cache_key)
		if not self.cache_dict:
			curr1 = get_object_or_404(Currency, code__iexact=self.kwargs['code1'])
			curr2 = get_object_or_404(Currency, code__iexact=self.kwargs['code2'])
			val1 = curr1.value
			val2 = curr2.value
			self.cache_dict = {'val1': val1, 'val2': val2, 'name1': curr1.name,
			                   'name2': curr2.name}
			cache.set(cache_key, self.cache_dict, 1800)
		result = val / self.cache_dict['val1'] * self.cache_dict['val2']
		return result


class InTxtView(LogicMixin, generic.View):

	def get(self, request, *args, **kwargs):
		try:
			result = self.get_result()
			return HttpResponse(result)
		except (Http404, SuspiciousOperation, ValueError) as e:
			return HttpResponse(e, status=400)


class InJsonView(LogicMixin, generic.View):

	def get(self, request, *args, **kwargs):
		try:
			result = self.get_result()
			return JsonResponse({'success': True, 'result': result})
		except (Http404, SuspiciousOperation, ValueError) as e:
			return JsonResponse({'success': False, 'error': str(e)}, status=400)


class InHTMLView(LogicMixin, MainView):

	def get_form(self):
		if self.request.method == 'GET':
			result = self.get_result()
			return self.form_class(initial={'val': self.kwargs['val'],
			                                'val2': result,
			                                'curr1': self.kwargs['code1'].upper(),
			                                'curr2': self.kwargs['code2'].upper()})
		else:
			return super(InHTMLView, self).get_form()

	def get_context_data(self, **kwargs):
		context = super(InHTMLView, self).get_context_data(**kwargs)
		context['name1'] = self.cache_dict['name1']
		context['name2'] = self.cache_dict['name2']
		return context
