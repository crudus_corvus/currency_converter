import requests
from .models import Currency

API_ROOT = 'https://openexchangerates.org/api/'
APP_ID = '5c9c2c9434e548f4b3fb488fb88f121a'


def fill():
    """initial database filling"""

    currencies_response = requests.get('{0}latest.json?app_id={1}'.format(API_ROOT, APP_ID))
    names_response = requests.get(API_ROOT + 'currencies.json')
    currencies_dictionary = currencies_response.json().get('rates')
    names_dictionary = names_response.json()
    for key, value in currencies_dictionary.items():
        currency_name = names_dictionary[key]
        Currency.objects.create(code=key, value=value, name=currency_name)


def update():
    """updating currencies values"""

    currencies_response = requests.get('{0}latest.json?app_id={1}'.format(API_ROOT, APP_ID))
    currencies_dictionary = currencies_response.json().get('rates')
    for key, value in currencies_dictionary.items():
        try:
            currency = Currency.objects.get(code=key)
            currency.value = value
            currency.save()
        except Currency.DoesNotExist:
            pass
